from django.urls import path
from ProyectoApp import views

urlpatterns = [
    path('inicio/', views.inicio, name = "INICIO"),
    path('tienda/', views.tienda, name = "TIENDA"),
    path('contacto/', views.contacto, name = "CONTACTO"),
]