from django.shortcuts import render, HttpResponse
from Servicios.models import Servicio
def inicio(request):
    return render(request,"ProyectoApp/inicio.html")


def tienda(request):
    return render(request,"ProyectoApp/tienda.html")

def contacto(request):
    return render(request,"ProyectoApp/contacto.html")
