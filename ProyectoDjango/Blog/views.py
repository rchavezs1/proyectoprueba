from django.shortcuts import render
from Blog.models import Post
def blog(request):
    post_s = Post.objects.all()
    return render(request,"blog/blog.html",{"posts":post_s})
